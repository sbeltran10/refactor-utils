const argv = require('minimist')(process.argv.slice(2))
const inputDir = argv.i

if (inputDir)
    switch (argv.op) {
        case 'rename-param-case':
            require('./utils/rename-param-case')(inputDir)
            break;
        default:
            break;
    }

else
    console.error("You must incude an input directory parameter")