const changeCase = require('change-case')
const fs = require('fs')

module.exports = inputDir => {
    if (fs.statSync(inputDir).isDirectory()) {
        recursiveRename(inputDir)
        console.log("Process complete")
    }
    else
        console.error("Not a directory")
}

const recursiveRename = dir => {
    let files = fs.readdirSync(dir)
    files.forEach(fileOrDir => {
        const path = dir + "/" + fileOrDir
        if (fs.statSync(path).isDirectory()) {
            recursiveRename(path)
        }
        rename(dir, fileOrDir)
    });
}

const rename = (dir, fileOrDir) => {
    let replacement = changeCase.paramCase(fileOrDir.split(".")[0])
    if (fs.statSync(dir + "/" + fileOrDir).isFile()) {
        replacement += "."
        replacement += fileOrDir.split(".")[1]
    }
    console.log("Renaming " + dir + "/" + fileOrDir + " to " + dir + "/" + replacement)
    fs.renameSync(dir + "/" + fileOrDir, dir + "/" + replacement)
}